require('dotenv').config()
const fetch = require('node-fetch');

const sendData = async () => {
    try {
        const options = {
            method: 'post',
            body: getPayload(),
            headers: { 'Content-Type': 'application/json' }
        }
        await fetch(process.env.HOST_URL, options)
        console.log("Data sended successfully")
    } catch (e) {
        console.log("An error occurred during send the data")
    }
}

const getPayload = () => JSON.stringify({
    timestamp: Date.now(),
    value: Math.random() * 100
})

setInterval(sendData, process.env.INTERVAL);